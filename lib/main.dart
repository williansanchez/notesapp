import 'package:flutter/material.dart';
import 'package:notas/src/bloc/provider.dart';
import 'package:notas/src/pages/home_page.dart';
import 'package:notas/src/pages/login_page.dart';
import 'package:notas/src/pages/notas_page.dart';
 
void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());

}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: 'login',
        routes: {
          'login'     : ( BuildContext context ) => LoginPage(),
          'home'      : ( BuildContext context ) => HomePage(),
          'notas'  : ( BuildContext context ) => notasPage(),
        },
        theme: ThemeData(
          primaryColor: Colors.blueAccent
        ),
      ),
    );
  }
}