import 'dart:io';

import 'package:notas/src/models/nots_model.dart';
import 'package:notas/src/providers/nots_provider.dart';
import 'package:rxdart/subjects.dart';

class notsBloc {

  final _notsController = new BehaviorSubject<List<notsModel>>();
  final _loadingController = new BehaviorSubject<bool>();

  final _notsProvider = new notsProvider();

  Stream<List<notsModel>> get notsStream => _notsController.stream;
  Stream<bool> get loading => _loadingController.stream;


  void loadnots() async{

    final nots = await _notsProvider.loadnots();
    _notsController.sink.add( nots );

  }

  void loadMynots() async{

    final nots = await _notsProvider.loadMynots();
    _notsController.sink.add( nots );

}