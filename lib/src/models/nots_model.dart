// To parse this JSON data, do
//
//     final notsModel = notsModelFromJson(jsonString);

import 'dart:convert';

notsModel notsModelFromJson(String str) => notsModel.fromJson(json.decode(str));

String notsModelToJson(notsModel data) => json.encode(data.toJson());

class notsModel {
    String nota;
    String materia;
    String userId;

    notsModel({
        this.nota = '',
        this.materia = '',
        this.userId,
    });

    factory notsModel.fromJson(Map<String, dynamic> json) => notsModel(
        nota        : json["nota"],
        materia        : json["materia"],
        userId      : json["userId"],
    );

    Map<String, dynamic> toJson() => {
        "nota"        : nota,
        "materia"        : materia,
        "userId"      : userId,
    };
}
