import 'package:flutter/material.dart';
import 'package:notas/src/bloc/provider.dart';
import 'package:notas/src/models/nots_model.dart';

class notasPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final notsBloc = Provider.notsBloc(context);
    notsBloc.loadnots();

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {Navigator.pushReplacementNamed(context, 'home'); },
        ),  
        title: const Text('notas'),
          }

  Widget _createList(notsBloc notsBloc) {

    return StreamBuilder(
      stream: notsBloc.notsStream ,
      builder: (BuildContext context, AsyncSnapshot<List<notsModel>> snapshot){
        if ( snapshot.hasData ) {

          final nots = snapshot.data;
          return ListView.builder(
            itemCount: nots.length,
            itemBuilder: ( context, i ) => _createItem( context, nots[i] ),
          );

        }
        else {
          return Center( child: CircularProgressIndicator() );
        }
      },
    );

  }

  }


}