import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:notas/src/models/nots_model.dart';
import 'package:notas/src/secure_storage/secure_storage.dart';
import 'package:mime_type/mime_type.dart';

class notsProvider {

  final _storage = new SessionStorage();
  final String _url = 'http://smartdev.uniajc.edu.co:8888/';

  Future<List<notsModel>> loadnots() async {
    
    final url = '$_url/nots/';
    final token = await _storage.getValueforKey('token');

    final resp = await http.get(
      url,
      headers: {
        'content-type' : 'application/json',
        'authorization' : token
      }
    );

    final Map<String,dynamic> decodedData = json.decode(resp.body);
    final List<notsModel> nots = new List();

    if ( decodedData['Data'] == null ) return [];

    decodedData['Data'].forEach( (not) {
      final notTemp = notsModel.fromJson(not);
      nots.add(notTemp);
    });

    return nots;

  }

  Future<List<notsModel>> loadMynots() async {
    
    final token = await _storage.getValueforKey('token');
    final userID = await _storage.getValueforKey('userId');
    final url = '$_url/nots/findByUser/$userID';

    final resp = await http.get(
      url,
      headers: {
        'content-type' : 'text',
        'authorization' : token
      }
    );

    final Map<String,dynamic> decodedData = json.decode(resp.body);
    final List<notsModel> nots = new List();

    if ( decodedData['Data'] == null ) return [];

    decodedData['Data'].forEach( (not) {
      final notTemp = notsModel.fromJson(not);
      nots.add(notTemp);
    });

    return nots;

  }

}