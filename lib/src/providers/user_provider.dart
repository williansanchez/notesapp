import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:notas/src/secure_storage/secure_storage.dart';

class UserProvider {


  Future<Map<String, dynamic>> login( String email, String password ) async {

    final _storage = new SessionStorage();

    final authData = {
      'email' : email,
      'password' : password
    };

    print( authData );

    final resp = await http.post(
      'http://smartdev.uniajc.edu.co:8888/users/login',
      headers: {
        'content-type' : 'application/json'
      },
      body: json.encode( authData )
    );
    
    Map<String, dynamic> decodedResp = json.decode( resp.body );

    print( decodedResp );

    if ( decodedResp['Status'] ) {
      await _storage.writeValue("token", decodedResp['Data']);
      await _storage.writeValue("userId", decodedResp['UserId']);
      return { 'ok': true, 'token': decodedResp['Data'] };
    }
    else{
      return { 'ok': false, 'message': 'Correo y/o contraseña incorrectos' };
    }

  }

}